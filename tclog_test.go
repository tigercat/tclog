package tclog

import (
	"testing"
)

func TestLogLdebug(t *testing.T) {
	SetOutputLevel(Ldebug)

	Debugf("Debug: foo\n")
	Debugln("Debug: foo")

	Infof("Info: foo\n")
	Infoln("Info: foo")

	Warnf("Warn: foo\n")
	Warnln("Warn: foo")

	Errorf("Error: foo\n")
	Errorln("Error: foo")

	Printf("Printf: foo\n")
	Println("Println: foo")
}

func TestLogLinfo(t *testing.T) {
	SetOutputLevel(Linfo)

	Debugf("Debug: foo\n")
	Debugln("Debug: foo")

	Infof("Info: foo\n")
	Infoln("Info: foo")

	Warnf("Warn: foo\n")
	Warnln("Warn: foo")

	Errorf("Error: foo\n")
	Errorln("Error: foo")

	Printf("Printf: foo\n")
	Println("Println: foo")
}
